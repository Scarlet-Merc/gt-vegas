import json
import discord
from discord.ext import commands

from .base import BasePlugin

# All plugins should inherit form BasePlugin
class autoreplyPlugin(BasePlugin):

    @commands.Cog.listener()
    async def on_message(self, message):
        if 'picrew' in message.channel.name :
            if message.content.lower().startswith("https://picrew.me/image_maker/"):
                await message.pin()

        elif "mod-vc" in message.channel.name:
            if message.content.lower().startswith("Question:"):
                await message.pin()

        elif "approved-vc" in message.channel.name:
            if message.content.lower().startswith("Question:"):
                await message.pin()

def setup(bot):
    bot.add_cog(autoreplyPlugin(bot))
